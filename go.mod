module modernc.org/sqlite

go 1.15

require (
	modernc.org/libc v1.1.1
	modernc.org/mathutil v1.1.1
	modernc.org/tcl v1.2.0
)
